Thank you for letting me know.
I must have done something wrong.
We've switched to [Runestone.Academy](runestone.academy) for the rest of the course.

You can use the Lambert PDF ebook as a resource, but from now on you will no longer need Cengage to complete the course.  Here's the link to the interactive eBook:

[**https://runestone.academy/ns/books/published/sdccd\_mesa\_college\_cs179\_spring23/index.html**](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/index.html)

If it's your first time using Runestone you will need to paste this course name into the "name of course" field during signup:

**sdccd\_mesa\_college\_cs179\_spring23**
