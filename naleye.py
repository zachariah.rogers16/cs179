#this is my travel the world game # Make the rooms/neighbors
shed_north = "forest"
shed_east = "desert"
shed_west = "mountains"
shed_south = "snow"
desert_north = "snow"
desert_west = "shed"
desert_east = "mountains"
forest_south = "shed"
forest_east = "snow"
mountains_north = "forest"
mountains_east = "desert"
snow_north = "forest"
snow_south = "shed"

# Make the commands
commands = ["north", "south", "east", "west"]

# Make the functions
def enter_shed():
    print("You are in the shed.")

def enter_desert():
    print("You are in the desert.")

def enter_forest():
    print("You are in the forest.")

def enter_snow():
    print("You are on snow.")

def enter_mountains():
    print("You are in the mountains.")

# Start the game
current_room = "shed"
print("Welcome to your journey! To begin, say 'let's go' or 'stay inside' to quit.")
response = input("What do you say? ").lower()
if response != "let's go":
    print("Exiting game.")
    exit()

# Start the game loop
while True:
    if current_room == "shed":
        enter_shed()
    elif current_room == "desert":
        enter_desert()
    elif current_room == "forest":
        enter_forest()
    elif current_room == "snow":
        enter_snow()
    elif current_room == "mountains":
        enter_mountains()

    user_input = input("Which way does your compass say? ").strip().lower()

    # Check the command
    if user_input not in commands:
        print("Invalid command.")
        continue

    # Check the request
    next_room = None
    if current_room == "shed":
        if user_input == "north":
            next_room = shed_north
        elif user_input == "east":
            next_room = shed_east
        elif user_input == "west":
            next_room = shed_west
        elif user_input == "south":
            next_room = shed_south
    elif current_room == "desert":
        if user_input == "north":
            next_room = desert_north
        elif user_input == "east":
            next_room = desert_east
        elif user_input == "west":
            next_room = desert_west
    elif current_room == "forest":
        if user_input == "south":
            next_room = forest_south
        elif user_input == "east":
            next_room = forest_east
        elif user_input == "north":
            next_room = mountains_north
    elif current_room == "snow":
        if user_input == "north":
            next_room = snow_north
        elif user_input == "south":
            next_room = snow_south
    elif current_room == "mountains":
        if user_input == "east":
            next_room = mountains_east
        elif user_input == "north":
            next_room = shed_north

    if next_room is None:
        print("You can't go that way.")
    else:
        current_room = next_room