import sys
def exit_game():
    print("Thank you for playing!")
    sys.exit()
print("Welcome to the Island of Gold!")
response = input("Would you like instructions? Please Enter Yes or No: ")
if response.strip().lower() == "yes":
    print("A group of explorers from England came all the way to the Island of Gold located off the shores of Thailand to look for the lost treasure behind a waterfall.")
else:
    print("Let's Begin!")
#room 1 is the waterfall cave entrance
response_2 = input("You are on a cliff you can chose to go down or go all the way around to get to the waterfall. Please Enter Down or Around: ")
if response_2.strip().lower() == "down":
    print("You picked the easy route. Good Job!")
else:
    print("Game Over!")
    exit_game()
#room 2(cave entrance)
response_3 = input("You have entered the waterfall and now must chose between two pathways to take. Enter 'left' or 'right': ")
if response_3.strip().lower() == "right":
    print("You chose the right pathway and reached a wooden door with a key inside the lock. It looks suspicious, but it is the fastest way to the gold.")
else:
    print("Game Over!")
    exit_game()
#room 3
response_5 = input("Think Fast! Should you Open the door or Go back';Enter open or go:")
if response_5.strip().lower() == "open":
    print("You entered the trap room and a boulder comes barreling towards you, but you moved out of it's way!")
elif response_5 == "back":
    print("By going back, it takes you longer to find the gold and everyone else leaves you no gold.")
else:
    print("Game Over!")
    exit_game()
#room 4 skeleton_room
response_6 = input("After the big boulder continued destruction, you began looking for clues and see a crumbled piece of paper in a skeleton hand's Enter 'grab' or exit':")
if response_6.strip().lower() == "grab":
    print("As you grabbed the paper from the skeleton's hand a spider popped out scaring you, but you picked it back up and began reading.")
else:
    print("You exit out of the room and find a different way to find the gold.")
response_7 = input("The paper said to solve the riddle; What is at the end of a rainbow?:(Enter:gold) ")
if response_7.strip().lower() == "gold":
    print("Congratulations you solved the riddle. You can move on to the final room!")
else:
    print("Game Over!")
    exit_game()
#room 6
response_8 = input("Should you open the last final door since you solved the riddle? Enter Yes or No")
if response_8.strip().lower() == "yes":
    print("Congratulations you are the first explorer to find the gold!")
else:
    print("Game Over!")
    exit_game()
